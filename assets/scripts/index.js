var heroes = {
    "superman": {
        "color": "rgb(47, 82, 194)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "<span>SUPER</span> TRABALHO!",
        "image": "superman.jpg"
    },
    "maravilha": {
        "color": "rgb(246, 51, 51)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "QUE <span>MARAVILHA</span>!",
        "image": "maravilha.jpg"
    },
    "batman": {
        "color": "rgb(6, 18, 88)",
        "colorspan": "rgb(255, 209, 3)",
        "title": "VOCÊ É IM<span>BAT</span>IVEL!",
        "image": "batman.png"
    },
    "avengers": {
        "color": "rgb(0, 150, 250)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "TIME <span>PODEROSO</span>!",
        "image": "avengers.jpg"
    },
    "naruto": {
        "color": "rgb(241, 137, 0)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "VOCÊ TEM MUITO <span>POTENCIAL</span>!",
        "image": "naruto.png"
    },
    "dart": {
        "color": "rgb(12, 12, 12)",
        "colorspan": "rgb(18, 131, 236)",
        "title": "OBRIGADO PELA <span>FORÇA</span>!",
        "image": "dart.jpg"
    },
    "goku": {
        "color": "rgb(255, 196, 0)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "QUE <span>TRANSFORMAÇÃO</span>!",
        "image": "goku.jpg"
    },
    "hulk": {
        "color": "rgb(4, 153, 54)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "QUE TRABALHO <span>INCRÍVEL</span>!",
        "image": "hulk.png"
    },
    "yoda": {
        "color": "rgb(8, 236, 0)",
        "colorspan": "rgb(0, 0, 0)",
        "title": "PARABÉNS <span>PADAWAN</span>!",
        "image": "yoda.jpg"
    }
};

$(function () {

    function setUp () {
        $(`.list-cards`).empty();
        Object.keys(heroes).forEach( function(key) {
            var item = heroes[key];
            $(`.list-cards`).append(`<div class="item-list-card ${key}"></div>`);
            var htmlElementClassKey = $(`.${key}`);
            htmlElementClassKey.css("background-color", item.color);
            htmlElementClassKey.append(item.title);
            htmlElementClassKey.click(function () {
                readCard(key);
            });
            $(`.${key} span`).css("color", item.colorspan);
        });
        $(`#submit`).click( function () {
            downloadCard();
        });
    }

    function readCard(hero) {
        var item = heroes[hero];
        $(`.card-header`).css("background-color", item.color);
        $(`.card-header`).empty();
        $(`.card-header`).append(`<h1>${item.title}<h1>`);
        $(`.card-header span`).css("color", item.colorspan);
        $(`.card-hero-image`).empty();
        $(`.card-hero-image`).append(`<img src="assets/img/${item.image}">`);
    }

    function downloadCard(){
        $("#hidded").empty();

        html2canvas(document.querySelector("#card"), {
            allowTaint: true,
            useCORS: false,            
            logging: false
        }).then( function(canvas) {
            canvas.getContext('2d');
            var link = document.createElement('a');
            link.download = 'card.png';
            link.href = canvas.toDataURL("image/png");
            link.click();
        });
        
    }

    function main() {
        $(document).ready(setUp);
        readCard("superman");
    }

    main();
});

